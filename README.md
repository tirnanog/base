TirNanoG Base Assets
====================

This asset set was originally based on LPC but diverged significantly, it's **ain't LPC**. TirNanoG Base is
the default starter kit for the [TirNanoG Editor](https://tirnanog.codeberg.page).

This package contains a basic configuration, user interface sprites, an extensive tile set, many character sheets, and a
[web-based character generator](https://tirnanog.codeberg.page/base) too (single index.html file, no dependencies, tested with
Firefox, should work with any browser). To expand it with new templates, the PNG file names must be added at the top in
lexicographical order, but files starting with `toolb` must come *after* `toolf`.

This JavaScript sheet generator is MIT licensed, everything else released under the terms of the **CC-BY-SA-4.0** license.


Cheers,

bzt
